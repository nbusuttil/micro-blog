export class PostsSerializer {

    deserializeModel(data) {
        return {
            title: data.title,
            body: data.body,
            author: data.author
        }
    }

    serializeModel(post) {
        return {
            title: post.title,
            body: post.body,
            author: post.author
        };
    }
}
