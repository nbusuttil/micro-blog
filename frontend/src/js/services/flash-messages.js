const defaultOptions = {
    trusted: false
};
const timeout = 40 * 1000;

export class FlashMessages {
    /* @ngInject */
    constructor($interval) {
        this.$interval = $interval;
        this.paused = false;
        this.messages = [];

        this.success = (message, options) => this.push('success', message, options);
        this.info = (message, options) => this.push('info', message, options);
        this.warning = (message, options) => this.push('warning', message, options);
        this.error = (message, options) => this.push('error', message, options);
    }

    push(type, message, options) {
        options = angular.extend({}, defaultOptions, options);
        var msg = {
            message: message,
            trusted: options.trusted,
            type: type
        };
        this.messages.push(msg);
        let promise = this.$interval(() => {
            if (!this.paused) {
                this.$interval.cancel(promise);
                this.messages.splice(this.messages.indexOf(msg), 1);
            }
        }, timeout);
    }

    remove(message) {
        var index = this.messages.indexOf(message);
        if (-1 !== index) {
            this.messages.splice(index, 1);
        }
    }

    resume() {
        this.paused = false;
    }

    pause() {
        this.paused = true;
    }

    isPaused() {
        return this.paused;
    }
}
