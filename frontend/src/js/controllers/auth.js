export class LoginController {
    /* @ngInject */
    constructor($firebaseAuth, $translate, $location, FlashMessages, UserService) {
        this.$translate = $translate;
        this.$location = $location;
        this.FlashMessages = FlashMessages;
        this.UserService = UserService;
        this.$firebaseAuth = $firebaseAuth;
        this.username = UserService.getUser();
        this.user = {};
        this.firebaseObj = new Firebase("https://busuttil.firebaseio.com");
        this.showSignIn = false;
    }

    signIn(event) {
        var loginObj = this.$firebaseAuth(this.firebaseObj);

        event.preventDefault();
            loginObj.$authWithPassword({
                email: this.user.email,
                password: this.user.password
            })
            .then((user) => {
                this.$translate('MICRO_BLOG.LOG.SUCCESS.LOGIN').then(this.FlashMessages.success);
                this.UserService.setUser(user.password.email);
                this.$location.path('/post');
            }, (error) => {
                this.$translate('MICRO_BLOG.LOG.ERROR.LOGIN').then(this.FlashMessages.error);
            });
    }

    signUp() {
        var auth = this.$firebaseAuth(this.firebaseObj),
            email = this.user.email,
            password = this.user.password;

        if (email && password) {
            auth.$createUser(this.user.email, this.user.password)
                .then(() => {
                    this.$translate('MICRO_BLOG.LOG.SUCCESS.LOGUP').then(this.FlashMessages.success);
                    this.$location.path('/post');
                }, (error) => {
                    this.$translate('MICRO_BLOG.LOG.ERROR.LOGIN').then(this.FlashMessages.error);
                });
        }

    }

    static template() {
        return `
            {{ ctrl.username }}
            <div class="container" ng-hide="ctrl.showSignIn">
                <div class="row">
                    <div class="col-lg-4 col-lg-offset-4 text-center">
                        <h2 translate="MICRO_BLOG.LOG.LOGIN"></h2>
                        <hr>
                        <form role="form" name="signIn">
                            <div class="form-group" ng-class="{ 'has-error' : signIn.email.$invalid }">
                                <input ng-model="ctrl.user.email" type="email" class="form-control" placeholder="{{ 'MICRO_BLOG.LOG.FORM.EMAIL' | translate }}">
                                <span class="text-danger pull-right" ng-show="signIn.email.$invalid" translate="MICRO_BLOG.LOG.ERROR.EMAIL"></span>
                            </div>
                            <div class="form-group" ng-class="{ 'has-error' : signIn.email.$invalid }">
                                <input ng-model="ctrl.user.password" type="password" class="form-control" placeholder="Password">
                                <span class="text-danger pull-right" ng-show="signIn.password.$error" translate="MICRO_BLOG.LOG.ERROR.PASSWORD"></span>
                            </div>
                            <hr>
                            <button ng-click="ctrl.signIn($event)" type="button" class="btn btn-default" translate="MICRO_BLOG.LOG.LOGIN"></button>
                            <button ng-click="ctrl.showSignIn=true" type="button" class="btn btn-default" translate="MICRO_BLOG.LOG.LOGUP.BTN"></button>
                        </form>
                    </div>
                </div>
            </div>

            <div class="container" ng-show="ctrl.showSignIn">
                <div class="row">
                    <div class="col-lg-4 col-lg-offset-4 text-center">
                        <h2 translate="MICRO_BLOG.LOG.LOGUP.TITLE"></h2>
                        <hr>
                        <form role="form" name="signUp">
                            <div class="form-group" ng-class="{ 'has-error' : signUp.email.$invalid }">
                                <input type="email" name="email" class="form-control" ng-model="ctrl.user.email" placeholder="{{ 'MICRO_BLOG.LOG.FORM.EMAIL' | translate }}">
                                <span class="text-danger pull-right" ng-show="signUp.email.$invalid" translate="MICRO_BLOG.LOG.ERROR.EMAIL"></span>
                            </div>
                            <div class="form-group" ng-class="{ 'has-error' : signUp.password.$invalid }">
                                <input type="password" name="password" class="form-control" ng-model="ctrl.user.password" placeholder="{{ 'MICRO_BLOG.LOG.FORM.PASSWORD' | translate }}">
                                <span class="text-danger pull-right" ng-show="signIn.password.$error" translate="MICRO_BLOG.LOG.ERROR.PASSWORD"></span>
                            </div>
                            <button ng-click="ctrl.signUp()" type="button" class="btn btn-default" translate="MICRO_BLOG.LOG.LOGIN"></button>
                        </form>
                    </div>
                </div>
            </div>
        `;
    }
}
