export class PostController {
    /* @ngInject */
    constructor($translate, $firebase, FlashMessages, UserService) {
        this.$translate = $translate;
        this.$firebase = $firebase;
        this.FlashMessages = FlashMessages;
        this.username = UserService.getUser();
        this.tab = 'blog';
        this.post = {};
    }

    addPost() {
        var firebaseObj = new Firebase("https://busuttil.firebaseio.com");
        var fb = this.$firebase(firebaseObj);

        console.log(fb);

        fb.$push({
            title: this.post.title,
            body: this.post.body,
            author: this.username
        }).then((ref) => {
            console.log(ref);
            this.$translate('MICRO_BLOG.NEW.FLASH_MESSAGE').then(this.FlashMessages.success);
        }, function(error) {
            console.log("Error:", error);
        });
    }

    static resolve() {
        return {}
    }
    static template() {
        return `
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                        <h2 translate="MICRO_BLOG.NEW.HEADER"></h2>
                        <hr>

                        <form name="postForm" ng-submit=" ctrl.addPost()" class="form-horizontal">
                            <div class="form-group">
                                <label translate="MICRO_BLOG.NEW.TITLE"></label>
                                <input class="form-control" type="text" ng-model="ctrl.post.title" required/>
                            </div>

                            <div class="form-group">
                                <label translate="MICRO_BLOG.NEW.CONTENT"></label>
                                <textarea class="form-control" ng-model="ctrl.post.body" ng-list="/\n/" rows="5" required></textarea>
                            </div>

                            <button type="submit" class="btn btn-default pull-right" ng-disabled="postForm.$invalid" translate="MICRO_BLOG.ACTIONS.SEND"></button>
                        </form>
                    </div>
                </div>
            </div>
            <uib-alert ng-repeat="alert in alerts" type="{{alert.type}}" close="closeAlert($index)">{{alert.msg}}</uib-alert>
        `;
    }
}
