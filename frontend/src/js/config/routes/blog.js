import {BlogController} from '../../controllers/blog'
import {PostController} from '../../controllers/post'
import {LoginController} from '../../controllers/auth'

export /* @ngInject */ function RouterMicroBlogConfig($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('blog', {
            url: '/',
            access: false,
            views: {
                '@': {
                    template: '<div ui-view></div>'
                }
            }
        })
        .state('blog.auth', {
            url: 'auth',
            access: false,
            views: {
                '@': {
                    template: LoginController.template(),
                    controller: LoginController,
                    controllerAs: 'ctrl'
                }
            }
        })
        .state('blog.post', {
            url: 'post',
            access: false,
            views: {
                '@': {
                    template: BlogController.template(),
                    controller: BlogController,
                    controllerAs: 'ctrl',
                    resolve: BlogController.resolve()
                }
            }
        })
        .state('blog.new', {
            url: 'new',
            access: true,
            views: {
                '@': {
                    template: PostController.template(),
                    controller: PostController,
                    controllerAs: 'ctrl',
                    resolve: PostController.resolve()
                }
            }
        });
}
