angular.module('blog', [
    'ui.router.compat',
    'ui.bootstrap',
    'angular-loading-bar',
    'pascalprecht.translate',
    'firebase'
]);

// Config
import {TranslateConfiguration} from './config/translate';
import {MicroBlogConfiguration} from './config/micro-blog';
angular
    .module('blog')
    .config(TranslateConfiguration)
    .run(MicroBlogConfiguration)
;

// Routes
import {RouterMicroBlogConfig} from './config/routes/blog';
angular
    .module('blog')
    .config(RouterMicroBlogConfig)
;

// Services
import {PostsSerializer} from './services/serializer/posts-serializer';
import {FlashMessages} from './services/flash-messages';
import {UserService} from './services/user';
angular
    .module('blog')
    .service('PostsSerializer', PostsSerializer)
    .service('FlashMessages', FlashMessages)
    .service('UserService', UserService)
;

//Directives
import {FlashMessagesDirective} from './directives/flash-messages';
angular
    .module('blog')
    .directive('supermanFlashMessages', FlashMessagesDirective)
;
