var gulp = require('gulp'),
    browserify = require('browserify'),
    babelify = require('babelify'),
    source = require('vinyl-source-stream'),
    sequence = require('run-sequence'),
    annotate = require('gulp-ng-annotate'),
    util = require('gulp-util'),
    less = require('gulp-less'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    templates = require('gulp-ng-templates'),
    fs = require('fs'),
    del = require('del');

var directories = {
    src: __dirname + '/src',
    public: __dirname + '/public',
    modules: __dirname + '/node_modules',
    libs: __dirname + '/libs',
    data: __dirname + '/data',
    tmp: __dirname + '/tmp'
};
var files = {
    es6: {
        entry: directories.src + '/js/app.js'
    },

    js: [
        directories.src + '/js/app.js',
        directories.src + '/js/**/*.js'
    ],

    vendor: [
        directories.modules + '/jquery/dist/jquery.min.js',
        directories.modules + '/angular/angular.min.js',
        directories.modules + '/angular-translate/dist/angular-translate.min.js',
        directories.modules + '/messageformat/messageformat.js',
        directories.modules + '/messageformat/locale/fr.js',
        directories.modules + '/angular-translate-interpolation-messageformat/angular-translate-interpolation-messageformat.min.js',
        directories.modules + '/angular-translate-handler-log/angular-translate-handler-log.min.js',
        directories.modules + '/angular-ui-router/release/angular-ui-router.min.js',
        directories.modules + '/angular-ui-bootstrap/ui-bootstrap.min.js',
        directories.modules + '/angular-ui-bootstrap/ui-bootstrap-tpls.min.js',
        '!' + directories.modules + '/angular-ui-utils/modules/**/test/**/*',
        '!' + directories.modules + '/angular-ui-utils/modules/**/demo/**/*',
        directories.modules + '/angular-loading-bar/build/loading-bar.min.js'
    ],

    firebase: [
        directories.libs + '/firebase.js',
        directories.libs + '/angularfire.js'
    ],

    less: [
        directories.src + '/less/main.less'
    ],

    fonts: [
        directories.modules + '/font-awesome/fonts/*',
        directories.modules + '/bootstrap/fonts/*'
    ],

    images: [
        directories.src + '/images/**',
        directories.modules + '/jquery-ui/themes/base/images/**'
    ],

    html: [
        directories.src + '/index.html'
    ],

    json: [
        directories.data + '/posts.json'
    ]
};

var config = {
    uglify: {
        mangle: true,
        compress: true,
        report: true
    }
};

if (fs.existsSync(directories.tmp) === false) {
    fs.mkdir(directories.tmp);
}
//Global Task
gulp.task('vendor', ['js:vendor', 'fonts:vendor', 'images:vendor']);
gulp.task('libs', ['js:firebase']);
gulp.task('app', ['js:app', 'styles:app:less', 'html:app', 'json:app']);

gulp.task('default', ['build']);
gulp.task('build', ['vendor', 'libs', 'app']);
gulp.task('dev', ['build', 'watch']);

//Clean Public
gulp.task('clean', function() {
    if (fs.existsSync(directories.public)) {
        del.sync(directories.public);
    }

    if (fs.existsSync(directories.modules)) {
        del.sync(directories.modules);
    }

    if (fs.existsSync(directories.modules)) {
        del.sync(directories.modules);
    }
});

//Build vendor JS
gulp.task('js:vendor', function() {
    var src = gulp.src(files.vendor)
        .pipe(concat('vendor.js'));

    if (process.env.NODE_ENV === 'production') {
        var cfg = config.uglify;
        cfg.mangle = false;

        src = src.pipe(uglify(config.uglify));
    }

    src.pipe(gulp.dest(directories.public + '/js'));
});

//Build firebase
gulp.task('js:firebase', function() {
    var src = gulp.src(files.firebase)
        .pipe(concat('firebase.js'));

    if (process.env.NODE_ENV === 'production') {
        var cfg = config.uglify;
        cfg.mangle = false;

        src = src.pipe(uglify(config.uglify));
    }

    src.pipe(gulp.dest(directories.public + '/js'));
});

//Build JS
gulp.task('js:app', function() {
    browserify(files.es6.entry, { debug: true })
        .transform(babelify)
        .bundle()
        .on('error', function(err) {
            console.log('Error: ' + err.message);
        })
        .pipe(source('app.js'))
        .pipe(annotate())
        .pipe(gulp.dest(directories.public + '/js'));
});

//Build Less
gulp.task('styles:app:less', function() {
    gulp.src(files.less)
        .pipe(less())
        .pipe(gulp.dest(directories.public + '/css'));
});

//Font Vendor
gulp.task('fonts:vendor', function() {
    gulp.src(files.fonts)
        .pipe(gulp.dest(directories.public + '/fonts'));
});

//Build Images
gulp.task('images:vendor', function() {
    gulp.src(files.images)
        .pipe(gulp.dest(directories.public + '/css/images'))
        .pipe(gulp.dest(directories.public + '/images'));
});

//Build html
gulp.task('html:app', function() {
    gulp.src(files.html)
        .pipe(gulp.dest(directories.public));
});

//Build json
gulp.task('json:app', function() {
    gulp.src(files.json)
        .pipe(gulp.dest(directories.public));
});

//Task watch
gulp.task('watch', function() {
    gulp.watch(files.js, ['js:app']);
    gulp.watch(files.vendor, ['js:vendor']);
    gulp.watch(files.less.concat(directories.src + '/less/**/*.less'), ['styles:app:less']);
    gulp.watch(files.fonts, ['fonts:vendor']);
    gulp.watch(files.images, ['images:vendor']);
    gulp.watch(files.html, ['html:app']);

    gulp.watch(__dirname + '/gulpfile.js', ['build']);
});
